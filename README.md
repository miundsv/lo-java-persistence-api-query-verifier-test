### How to use code in Eclipse?

Import the two Java projects in this repo into your workspace using the "existing projects into workspace" option in the package explorer "Import..." menu (Open by right click).

The build path of both projects is setup with an Eclipse projects called "Model" and "Logic". You can remove those; they are solely suggestions for how you could separate your code into different projects. The same accounts for the *se.miun.dsv.jee.jpa.Event* class. You can substitute it by a class of your choice.
